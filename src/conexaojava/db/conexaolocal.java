/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package conexaojava.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class conexaolocal {
    private static final String STR_CONEXAO = "jdbc:postgresql://127.0.0.1/aula";
    private static final String DRIVER = "org.postgresql.Driver";
    //private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String USUARIO = "aula";
    private static final String SENHA = "aula";
    private Connection c;
    private static conexaolocal minhaConexao;
   
    public static Connection getConnection(){
        if(minhaConexao == null){
            minhaConexao = new conexaolocal();

        }
        minhaConexao.conexao();
        return minhaConexao.c;
    }
   
    private void conexao(){
        try{
            Class.forName(DRIVER).newInstance();
            this.c = DriverManager.getConnection(STR_CONEXAO,USUARIO,SENHA);
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException ex) {
            JOptionPane.showMessageDialog(null,"Conexão não estabelecida\n "+ex.getMessage(),"Erro na Conexão com o SGDB",JOptionPane.INFORMATION_MESSAGE);
           
        }
    }
   /*
    public void closeConnection() throws Exception{
        connection.close();
    }*/
}
