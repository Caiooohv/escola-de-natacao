/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package conexaojava.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class insercao {
    public void inserir(String nome, String endereco, String email, String CPF, String telefone){
        try {
            Connection c = conexaomeu.obterConexao();
            PreparedStatement ps = c.prepareStatement("insert into sc_natacao.aluno(cpf, nome, email, telefone, endereco) values(?,?,?,?,?)");
            ps.setString(1, CPF);
            ps.setString(2, nome);
            ps.setString(3, email);
            ps.setString(4, telefone);
            ps.setString(5, endereco);
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void inserir2(String cpf,String nome, String telefone){
        try {
            Connection c = conexaomeu.obterConexao();
            PreparedStatement ps = c.prepareStatement("insert into sc_natacao.professor(cpf, nome, telefone) values(?,?,?)");
            ps.setString(1, cpf);
            ps.setString(2, nome);
            ps.setString(3, telefone);
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void inserirTurma(String professor, String horario, String dias){
        try {
            Connection c = conexaomeu.obterConexao();
            PreparedStatement ps = c.prepareStatement("insert into sc_natacao.turma(professor, horario, dias) values(?,?,?)");
            ps.setString(1, professor);
            ps.setString(2, horario);
            ps.setString(3,dias);
            System.out.println(ps);
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
