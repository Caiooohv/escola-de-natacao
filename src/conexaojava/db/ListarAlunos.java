/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package conexaojava.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * 
 * @author CaioVieira
 */
public class ListarAlunos {
    public void listar(JList listaAlunos){
        try {
            listaAlunos.removeAll();
            DefaultListModel dfm = new DefaultListModel();
            Connection c = conexao.getConnection();
            String SQL = "select * from sc_natacao.aluno;";
            PreparedStatement ps = c.prepareStatement(SQL);
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(SQL);
            while(rs.next()){
                dfm.addElement(rs.getString("nome"));
                
            }
            listaAlunos.setModel(dfm);//conecta o resultado a lista
            c.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(ListarAlunos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void listarProf(JList listaAlunos){
        try {
            listaAlunos.removeAll();
            DefaultListModel dfm = new DefaultListModel();
            Connection c = conexao.getConnection();
            String SQL = "select * from sc_natacao.professor;";
            PreparedStatement ps = c.prepareStatement(SQL);
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(SQL);
            while(rs.next()){
                dfm.addElement(rs.getString("nome"));
                
            }
            listaAlunos.setModel(dfm);//conecta o resultado a lista
            c.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(ListarAlunos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void detalhar(JTable tabela) throws SQLException{
        try {
            DefaultTableModel dtm = new DefaultTableModel();
            dtm = (DefaultTableModel)tabela.getModel();
            Connection c = conexao.getConnection();
            String SQL = "select * from sc_natacao.aluno";
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(SQL);
            while(rs.next()){
                dtm.addRow(new Object[]{
                    rs.getString("nome"), rs.getString("Email"), rs.getString("Telefone"),rs.getString("CPF")});
                
            }
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Listagem.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    public void listProfCombo(JComboBox combo){
        try {
            DefaultComboBoxModel d = new DefaultComboBoxModel();
            Connection c = conexaomeu.obterConexao();
            PreparedStatement ps = c.prepareStatement("select * from sc_natacao.professor;");
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Turma a = new Turma(rs.getString("nome"), rs.getString("cpf"), rs.getString("telefone"));
                d.addElement(a);
            }
            combo.setModel(d);
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Turma.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
