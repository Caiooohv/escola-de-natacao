/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package conexaojava.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class conexao {
    private static final String STR_CONEXAO = "jdbc:postgresql://10.90.24.54/caiohvieira";
    private static final String DRIVER = "org.postgresql.Driver";
    //private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String USUARIO = "caio_hvieira";
    private static final String SENHA = "senha";
    private Connection c;
    private static conexao minhaConexao;
   
    public static Connection getConnection(){
        if(minhaConexao == null){
            minhaConexao = new conexao();

        }
        minhaConexao.conexao();
        return minhaConexao.c;
    }
   
    private void conexao(){
        try{
            Class.forName(DRIVER).newInstance();
            this.c = DriverManager.getConnection(STR_CONEXAO,USUARIO,SENHA);
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException ex) {
            JOptionPane.showMessageDialog(null,"Conexão não estabelecida\n "+ex.getMessage(),"Erro na Conexão com o SGDB",JOptionPane.INFORMATION_MESSAGE);
           
        }
    }
   /*
    public void closeConnection() throws Exception{
        connection.close();
    }*/
}
